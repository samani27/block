<?php

class block_participantschart_edit_form extends block_edit_form {

    public static $type = array("totalparticipants" => "Total Participants per Course",
                                        "totalcourse" => "Total Course that Participants");
   
    protected function specific_definition($form) {

        $form->addElement('header', 'configheader', get_string('blocksettings', 'block'));

        $option = ['allparticipant'=>'Total Participants Per Course', 'completecourse' => 'Total Participants who Completed the Course'];

        $form->addElement('select', 'config_charttype', get_string('chart_type', 'block_participantschart'), $option);
        
    }
}
