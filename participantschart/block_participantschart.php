<?php

require_once(dirname(__FILE__) . '/../../config.php');
// require_once($CFG->dirroot.'/blocks/completion_progress/lib.php');

defined('MOODLE_INTERNAL') || die;


class block_participantschart extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_participantschart');
    }

    function has_config() {
        return true;
    }

    function get_content() {
        global $CFG, $USER, $DB, $OUTPUT;

        if($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';

         if (!empty($this->config->text)) {
            $this->content->text = $this->config->text;
        } else 
        {
             $this->content->text = '';
             $this->content->text .= HTML_WRITER::tag('div', '', array('id'=>"container"));

             $this->page->requires->js('/blocks/participantschart/thirdparty/highcharts.js', true);
            $type = '';
            if(isset($this->config) && $this->config->charttype == 'completecourse'){
                $type = $this->config->charttype;
                $data  = $this->get_complete_course();
                $category = $data['category'];
                $result = $data['result'];
                $title = 'Total participants who completed the course';
            } else {
                $result = $this->get_participants();
                $category = '';
                $title = 'Total Participants Per Course';
            }

          
            $this->page->requires->js_call_amd('block_participantschart/chartrender', 'drawChart', array($result, $title, $category, $type));
    
            
        }

        return $this->content;
    }


    private function get_participants()
    {
        global $DB;

        $sql = "(
                    SELECT
                    c.fullname, 
                    count(u.id)                                
                    FROM 
                    {role_assignments} ra 
                    JOIN {user} u ON u.id = ra.userid
                    JOIN {role} r ON r.id = ra.roleid
                    JOIN {context} cxt ON cxt.id = ra.contextid
                    JOIN mdl_course c ON c.id = cxt.instanceid
                    WHERE ra.userid = u.id             
                    AND ra.contextid = cxt.id
                    AND cxt.contextlevel =50
                    AND cxt.instanceid = c.id
                    AND  roleid = 5
                    group by c.id
                    ORDER BY c.fullname
                    
                )
                UNION 
                (
                    SELECT fullname, 0 as total from (
                                SELECT courseid, ue.id as enrol_id 
                                FROM mdl_user_enrolments ue
                                LEFT JOIN mdl_enrol e on e.id=ue.enrolid
                            ) t
                    RIGHT JOIN mdl_course mc on mc.id=t.courseid
                    WHERE t.courseid is null

               )
        ";
        $result = $DB->get_records_sql_menu($sql);
       
       $participants = array();
       
       foreach ($result as $key => $value) {
          
            $participants[] = array($key, (int)$value);    
            
       }

        return $participants;
    }


    private function get_complete_course()
    {

        global $DB;

        $sql = 'SELECT
                    c.fullname, 
                    count(u.id)                                
                    FROM 
                    {role_assignments} ra 
                    JOIN {user} u ON u.id = ra.userid
                    JOIN {role} r ON r.id = ra.roleid
                    JOIN {context} cxt ON cxt.id = ra.contextid
                    JOIN mdl_course c ON c.id = cxt.instanceid
                    WHERE ra.userid = u.id             
                    AND ra.contextid = cxt.id
                    AND cxt.contextlevel =50
                    AND cxt.instanceid = c.id
                    AND  roleid = 5
                    group by c.id
                    ORDER BY c.fullname
                ';

        $sql_complete = 'SELECT
                    c.fullname, 
                    count(u.id)                                
                    FROM 
                    {role_assignments} ra 
                    JOIN {user} u ON u.id = ra.userid
                    JOIN {role} r ON r.id = ra.roleid
                    JOIN {context} cxt ON cxt.id = ra.contextid
                    JOIN {course} c ON c.id = cxt.instanceid
                    JOIN {course_completions} cc on cc.course=c.id and u.id=cc.userid
                    WHERE ra.userid = u.id       
                    AND ra.contextid = cxt.id
                    AND cxt.contextlevel =50
                    AND cxt.instanceid = c.id
                    AND  roleid = 5
                    and timecompleted is not null
                    group by c.id
                    ORDER BY c.fullname
                    ';


        $all_participant = $DB->get_records_sql_menu($sql);
        $complete_participant = $DB->get_records_sql_menu($sql_complete);
       

       $participant = array();

       $value_all = array();
       $value_complete = array();
       $category = array();
       foreach ($all_participant as $key => $value) {
            $category[] = $key;
            $value_all[] = (int)$value;

            if(array_key_exists($key, $complete_participant)){
                $value_complete[] = (int) $complete_participant[$key];
            } else {
                $value_complete[] = 0;
            }
            
       }

        $data_all = array('name' => 'All Participants',
                            'data' => $value_all);
        $data_complete = array('name' => 'Passed Participants',
                            'data' => $value_complete);

        $result['result'] = array($data_all, $data_complete);
        $result['category'] = $category;

        return $result;
    }

}
